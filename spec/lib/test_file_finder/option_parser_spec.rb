# frozen_string_literal: true

RSpec.describe TestFileFinder::OptionParser do
  subject(:options) { described_class.parse!(argv) }

  describe '--mapping-file FILE' do
    let(:argv) { %w[--mapping-file mapping.yml] }

    it { expect(options.mapping_file).to eq('mapping.yml') }
  end

  describe '-f FILE' do
    let(:argv) { %w[-f mapping.yml] }

    it { expect(options.mapping_file).to eq('mapping.yml') }
  end

  describe '--yaml FILE' do
    let(:argv) { %w[--yaml mapping.yml] }

    it { expect(options.mapping_file).to eq('mapping.yml') }
  end

  describe '--json FILE' do
    let(:argv) { %w[--json mapping.json] }

    it { expect(options.json).to eq('mapping.json') }
  end

  describe '--project-path PROJECT_PATH' do
    let(:argv) { %w[--project-path gitlab-org/gitlab] }

    it { expect(options.project_path).to eq('gitlab-org/gitlab') }
  end

  describe '--merge-request-iid MERGE_REQUEST_IID' do
    let(:argv) { %w[--merge-request-iid 123] }

    it { expect(options.merge_request_iid).to eq(123) }
  end
end

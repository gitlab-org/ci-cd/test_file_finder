# frozen_string_literal: true

RSpec.describe TestFileFinder::MappingStrategies::PatternMatching do
  let(:instance) { described_class.new }

  let(:yaml) do
    <<~YAML
      mapping:
        - source: app/(.+)\.rb
          test: spec/%s_spec.rb
        - source: lib/(.+)\.rb
          test: spec/lib/%s_spec.rb
        - source:
            - db/schema.rb
            - db/migrate/(.+)
          test: spec/db/schema_spec.rb
        - source: spec/(.+)_spec\.rb
          test:
            - spec/%s_spec.rb
            - spec/all_spec.rb
    YAML
  end

  let(:test_mapping_file) { 'test_mapping.yml' }

  before do
    allow(YAML).to receive(:safe_load_file).with(test_mapping_file).and_return(YAML.safe_load(yaml))
  end

  describe '.load' do
    let(:mapping) { described_class.load(test_mapping_file) }

    it 'loads test_map file into a Mapping' do
      expect(mapping.pattern_matchers.size).to eq(6)
    end

    context 'with incorrect yaml' do
      let(:yaml) do
        <<~YAML
          maping:
            - source: app/(.+)\.rb
              test: spec/%s_spec.rb
        YAML
      end

      it 'raises error' do
        expect { mapping }.to raise_error(
          TestFileFinder::InvalidMappingFileError, 'missing `mapping` in test mapping file'
        )
      end
    end

    context 'with incomplete mapping' do
      let(:yaml) do
        <<~YAML
          mapping:
            - source: app/(.+)\.rb
              test: spec/%s_spec.rb
            - source: app/models/(.+)\.rb
        YAML
      end

      it 'rejects with error' do
        expect { mapping }.to raise_error(
          TestFileFinder::InvalidMappingFileError, 'missing `source` or `test` in test mapping file'
        )
      end
    end
  end

  describe '.default_rails_mapping' do
    subject(:output) { described_class.default_rails_mapping.match(paths) }

    context 'with a Rails model path' do
      let(:paths) { 'app/models/widget.rb' }

      it { is_expected.to eq(%w[spec/models/widget_spec.rb]) }
    end

    context 'with a nested Rails model path' do
      let(:paths) { 'app/models/group/widget.rb' }

      it { is_expected.to eq(%w[spec/models/group/widget_spec.rb]) }
    end

    context 'with a Rails lib path' do
      let(:paths) { 'lib/helpfulness.rb' }

      it { is_expected.to eq(%w[spec/lib/helpfulness_spec.rb]) }
    end

    context 'with a nested Rails lib path' do
      let(:paths) { 'app/lib/specific/helpfulness.rb' }

      it { is_expected.to eq(%w[spec/lib/specific/helpfulness_spec.rb]) }
    end

    context 'with a model spec file' do
      let(:paths) { 'spec/models/widget_spec.rb' }

      it 'returns the file path unmodified' do
        expect(output).to eq([paths])
      end
    end

    context 'with multiple paths' do
      subject(:output_multiple_paths) do
        paths.flat_map do |path|
          described_class.default_rails_mapping.match(path)
        end
      end

      context 'when they all match' do
        let(:paths) { %w[app/models/widget.rb app/models/group/widget.rb spec/models/related_model_spec.rb] }
        let(:expected_paths) do
          %w[spec/models/widget_spec.rb spec/models/group/widget_spec.rb spec/models/related_model_spec.rb]
        end

        it { is_expected.to eq(expected_paths) }
      end

      context 'when they do not match' do
        let(:paths) { %w[unknown_context/file.rb vendor/stuff.rb] }

        it { is_expected.to eq([]) }
      end

      context 'when they partially match' do
        let(:paths) { %w[app/models/widget.rb unknown_context/file.rb] }

        it 'ignores the file of unknown context' do
          expect(output_multiple_paths).to eq(%w[spec/models/widget_spec.rb])
        end
      end
    end
  end

  describe '#relate' do
    it 'adds a pattern matcher from source and test file patterns' do
      source = 'app/(.+)\.rb'
      test1 = 'spec/%s_spec.rb'
      test2 = 'lib/spec/%s_spec.rb'

      instance.relate(source, test1)
      instance.relate(source, test2)

      expect(instance.pattern_matchers.length).to eq(2)
    end
  end

  describe '#match' do
    it 'returns matching test file names' do
      source = 'app/(.+)\.rb'
      test = 'spec/%s_spec.rb'
      file = 'app/model/project.rb'

      instance.relate(source, test)

      expect(instance.match(file)).to contain_exactly('spec/model/project_spec.rb')
    end

    context 'with multiple matching test files' do
      it 'returns all matching test file names' do
        source = 'app/(.+)\.rb'
        test_1 = 'spec/%s_spec.rb'
        test_2 = 'ee/spec/%s_spec.rb'
        file = 'app/model/project.rb'

        instance.relate(source, test_1)
        instance.relate(source, test_2)

        expect(instance.match(file)).to contain_exactly('spec/model/project_spec.rb', 'ee/spec/model/project_spec.rb')
      end
    end

    context 'when the file does not match given pattern' do
      it 'returns nothing' do
        source = 'app/(.+)\.rb'
        test = 'spec/%s_spec.rb'
        file = 'lib/model/project.rb'

        instance.relate(source, test)

        expect(instance.match(file)).to be_empty
      end
    end

    context 'with multiple matching mappings' do
      it 'returns all matching test file names' do
        source_1 = 'ee/app/(.+)\.rb'
        test_1 = "ee/spec/%s_spec.rb"
        source_2 = 'ee/app/(.*/)ee/(.+)\.rb'
        test_2 = "ee/spec/%s%s_spec.rb"
        file = 'ee/app/models/ee/user.rb'

        instance.relate(source_1, test_1)
        instance.relate(source_2, test_2)

        expect(instance.match(file)).to contain_exactly('ee/spec/models/ee/user_spec.rb', 'ee/spec/models/user_spec.rb')
      end
    end

    context 'with matching subdirectory' do
      it 'returns nothing' do
        source = 'app/(.+)\.rb'
        test = 'spec/%s_spec.rb'
        file = 'ee/app/model/project.rb'

        instance.relate(source, test)

        expect(instance.match(file)).to be_empty
      end
    end

    context 'with multiple files' do
      it 'returns all matching test file names' do
        source = 'app/(.+)\.rb'
        test = 'spec/%s_spec.rb'
        file1 = 'app/model/project.rb'
        file2 = 'app/model/user.rb'

        instance.relate(source, test)

        expect(instance.match([file1,
          file2])).to contain_exactly('spec/model/project_spec.rb', 'spec/model/user_spec.rb')
      end
    end

    context 'with repeated matching test files' do
      it 'returns all unique test file names' do
        source = 'app/(.+)\.rb'
        test1 = 'spec/%s_spec.rb'
        test2 = 'spec/model/project_spec.rb'
        file = 'app/model/project.rb'

        instance.relate(source, test1)
        instance.relate(source, test2)

        expect(instance.match(file)).to contain_exactly('spec/model/project_spec.rb')
      end
    end

    context 'with a non-regex string' do
      it 'returns test file name' do
        source = 'db/structure.sql'
        test = 'spec/db_schema_spec.rb'

        instance.relate(source, test)

        expect(instance.match(source)).to contain_exactly(test)
      end
    end

    context 'with regexp with named captures' do
      it 'returns all unique test file names' do
        source = 'lib/api/(?<name>.*)\.rb'
        test = 'spec/requests/api/%{name}/%{name}_spec.rb'
        file = 'lib/api/issues.rb'

        instance.relate(source, test)

        expect(instance.match(file)).to contain_exactly('spec/requests/api/issues/issues_spec.rb')
      end
    end

    context 'with regexp with mixed captures' do
      it 'fails to match numbered and named captures' do
        source = 'lib/api/(?<name>.*)\.rb'
        test = 'spec/requests/api/%{name}/%s_spec.rb'
        file = 'lib/api/issues.rb'

        instance.relate(source, test)

        expect do
          instance.match(file)
        end.to raise_error(ArgumentError, 'unnumbered(1) mixed with named')
      end
    end
  end
end

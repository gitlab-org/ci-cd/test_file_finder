# frozen_string_literal: true

require 'json'

RSpec.describe TestFileFinder::MappingStrategies::DirectMatching do
  let(:json) do
    <<~JSON
      {
        "app/models/project.rb": [
          "spec/models/project_spec.rb",
          "spec/controllers/projects_controller_spec.rb"
        ],
        "app/controllers/projects_controller.rb": [
           "spec/controllers/projects_controller_spec.rb"
        ]
      }
    JSON
  end

  let(:file_name)        { 'mapping.json' }
  let(:limit_percentage) { nil }
  let(:limit_min)        { nil }

  before do
    allow(File).to receive(:read).with(file_name).and_return(json)
  end

  describe '.load_json' do
    let(:mapping_strategy) do
      described_class.load_json(file_name, limit_percentage: limit_percentage, limit_min: limit_min)
    end

    it 'loads json file into a DirectMatching strategy' do
      expect(mapping_strategy).to be_a(described_class)
    end

    it 'loads json into mapping' do
      expect(mapping_strategy.map).to eq(JSON.parse(json))
    end

    context 'when given an invalid JSON mapping' do
      let(:json) do
        <<~JSON
          []
        JSON
      end

      it 'raises an error' do
        expect { mapping_strategy }.to raise_error(
          TestFileFinder::InvalidMappingFileError, described_class::JSON_ERROR_MESSAGE
        )
      end
    end

    context 'when given a json object that does not have array as values' do
      let(:json) do
        <<~JSON
          {
            "file": {}
          }
        JSON
      end

      it 'raises error' do
        expect { mapping_strategy }.to raise_error(
          TestFileFinder::InvalidMappingFileError, described_class::JSON_ERROR_MESSAGE
        )
      end
    end

    context 'when limit_percentage is set' do
      context 'when the value is a string' do
        let(:limit_percentage) { '40' }

        it 'raises an error' do
          expect { mapping_strategy }.to raise_error(
            'Invalid value for limit_percentage: should be an integer between 1 and 100'
          )
        end
      end

      context 'when the value is a float' do
        let(:limit_percentage) { 40.0 }

        it 'raises an error' do
          expect { mapping_strategy }.to raise_error(
            'Invalid value for limit_percentage: should be an integer between 1 and 100'
          )
        end
      end

      context 'when the value is outside the allowed range' do
        let(:limit_percentage) { 110 }

        it 'raises an error' do
          expect { mapping_strategy }.to raise_error(
            'Invalid value for limit_percentage: should be an integer between 1 and 100'
          )
        end
      end

      context 'when the value is valid' do
        let(:limit_percentage) { 50 }

        context 'when limit_min is set' do
          context 'when the value is not a number' do
            let(:limit_min) { '3' }

            it 'raises an error' do
              expect { mapping_strategy }.to raise_error(
                'Invalid value for limit_min: should be an integer strictly greater than zero'
              )
            end
          end

          context 'when the value is a negative number' do
            let(:limit_min) { -3 }

            it 'raises an error' do
              expect { mapping_strategy }.to raise_error(
                'Invalid value for limit_min: should be an integer strictly greater than zero'
              )
            end
          end

          context 'when the value is zero' do
            let(:limit_min) { 0 }

            it 'raises an error' do
              expect { mapping_strategy }.to raise_error(
                'Invalid value for limit_min: should be an integer strictly greater than zero'
              )
            end
          end

          context 'when the value is valid' do
            it 'returns an instance' do
              expect(mapping_strategy).to be_an_instance_of(described_class)
            end
          end
        end
      end
    end
  end

  describe '#match' do
    let(:limit_percentage) { nil }
    let(:limit_min) { nil }
    let(:instance) { described_class.new(parsed_json, limit_percentage: limit_percentage, limit_min: limit_min) }
    let(:file1) { 'app/models/project.rb' }
    let(:file2) { 'app/controllers/projects_controller.rb' }
    let(:test1) { 'spec/models/project_spec.rb' }
    let(:test2) { 'spec/controllers/projects_controller_spec.rb' }
    let(:test3) { 'spec/controllers/another_projects_controller_spec.rb' }
    let(:test_files_for_files_1) { [test1, test2, test3] }
    let(:test_files_for_files_2) { [test2] }
    let(:parsed_json) do
      {
        file1 => test_files_for_files_1,
        file2 => test_files_for_files_2
      }
    end

    it 'returns an array' do
      expect(instance.match(file1)).to be_a(Array)
    end

    it 'returns matching test file names' do
      expect(instance.match(file1)).to contain_exactly(test1, test2, test3)
      expect(instance.match(file2)).to contain_exactly(test2)
    end

    context 'with multiple files' do
      it 'returns all matching test file names' do
        expect(instance.match([file1, file2])).to contain_exactly(test1, test2, test3)
      end
    end

    context 'with unknown file' do
      it 'returns empty array' do
        expect(instance.match('unkown')).to be_empty
      end
    end

    context 'when a valid limit_percentage is set' do
      let(:limit_percentage)           { 50 }
      let(:number_of_tests_for_file_1) { (test_files_for_files_1.count * (limit_percentage / 100.0)).round }

      context 'when a valid limit_min is set' do
        context 'when the limit is higher than the percentage count' do
          let(:limit_min) { 100 }

          it 'returns the correct number of items' do
            expect(test_files_for_files_1).to receive(:sample).with(limit_min).and_call_original

            instance.match(file1)
          end
        end

        context 'when the limit is lower than the percentage count' do
          let(:limit_min) { 1 }

          it 'returns the correct number of items' do
            expect(test_files_for_files_1).to receive(:sample).with(number_of_tests_for_file_1).and_call_original

            instance.match(file1)
          end
        end
      end

      it 'returns the correct number of items' do
        expect(test_files_for_files_1).to receive(:sample).with(number_of_tests_for_file_1).and_call_original

        instance.match(file1)
      end
    end
  end
end

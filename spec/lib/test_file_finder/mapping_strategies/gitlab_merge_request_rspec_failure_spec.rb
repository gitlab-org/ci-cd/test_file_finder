# frozen_string_literal: true

RSpec.describe TestFileFinder::MappingStrategies::GitlabMergeRequestRspecFailure do
  let(:project_path) { 'gitlab-org/gitlab' }
  let(:merge_request_iid) { 99 }
  let(:test_reports) { File.read("#{__dir__}/../../../../fixtures/test_reports.json") }

  describe '#match' do
    subject(:output) { described_class.new(project_path: project_path, merge_request_iid: merge_request_iid).match }

    let(:stub_api) do
      stub_request(:get, "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_iid}/test_reports.json")
        .to_return(status: 200, body: test_reports)
    end

    before do
      stub_api
    end

    it 'calls the merge request test reports api once' do
      output

      expect(stub_api).to have_been_requested.once
    end

    it 'returns the file names of new failures and existing failures' do
      expected_files = %w[
        spec/app/rspec_unit_pg11_spec.rb
        spec/features/rspec_system_pg11_as_if_foss_spec.rb
        ee/spec/rspec_integration_pg11_spec.rb
        ee/spec/features/rspec_system_pg11_spec.rb
      ]

      expect(output).to match_array(expected_files)
    end

    it 'does not return the file name of resolved failures' do
      resolved_tests = %w[
        ee/spec/features/resolved/rspec_system_pg11_spec.rb
        spec/resolved/rspec_unit_pg11_spec.rb
      ]

      expect(output).not_to include(*resolved_tests)
    end

    it 'does not return non ruby specs' do
      expect(output).not_to include('spec/frontend/javascript_spec.js')
    end

    context 'when test report URL returns 204' do
      let(:stub_api) do
        stub_request(:get, "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_iid}/test_reports.json")
          .to_return(status: 204)
      end

      it 'raises error' do
        expect do
          output
        end.to raise_error(TestFileFinder::TestReportError,
          "Test report for merge request #{merge_request_iid} is not ready, please try again later.")
      end
    end

    context 'when test report URL returns 400' do
      let(:stub_api) do
        stub_request(:get, "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_iid}/test_reports.json")
          .to_return(status: 400)
      end

      it 'raises error' do
        expect do
          output
        end.to raise_error(TestFileFinder::TestReportError,
          "The project #{project_path} does not have test reports configured.")
      end
    end

    context 'when test report URL returns 500' do
      let(:stub_api) do
        stub_request(:get, "https://gitlab.com/#{project_path}/-/merge_requests/#{merge_request_iid}/test_reports.json")
          .to_return(status: 500)
      end

      it 'raises error' do
        expect do
          output
        end.to raise_error(TestFileFinder::TestReportError, 'Unable to retrieve test report, please try again later.')
      end
    end
  end
end
